Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/941b3d562238e25c3e65ca8c2483c9c8ec3ba2040d9a772eb59136b0daec112b.jpg: 640x480 1 sports ball, 2390.8ms
Speed: 2.9ms preprocess, 2390.8ms inference, 2.8ms postprocess per image at shape (1, 3, 640, 480)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
