Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/d046fe89912ad238e124041960f1e41259ddfa16cd0945c702c286b54b8cce2c.jpg: 640x480 (no detections), 2527.7ms
Speed: 2.7ms preprocess, 2527.7ms inference, 1.3ms postprocess per image at shape (1, 3, 640, 480)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
