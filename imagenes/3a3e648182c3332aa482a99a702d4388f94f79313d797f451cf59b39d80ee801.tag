Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/3a3e648182c3332aa482a99a702d4388f94f79313d797f451cf59b39d80ee801.jpg: 576x640 (no detections), 3119.5ms
Speed: 4.8ms preprocess, 3119.5ms inference, 1.6ms postprocess per image at shape (1, 3, 576, 640)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
