Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/c1da45417742f9de0b118ff02e7657e85d66c19ed3f8287b634466c8277c740a.jpg: 640x448 1 person, 2315.3ms
Speed: 3.2ms preprocess, 2315.3ms inference, 1.6ms postprocess per image at shape (1, 3, 640, 448)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
