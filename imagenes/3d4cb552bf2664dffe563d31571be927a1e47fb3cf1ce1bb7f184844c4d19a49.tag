Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/3d4cb552bf2664dffe563d31571be927a1e47fb3cf1ce1bb7f184844c4d19a49.jpg: 448x640 3 cats, 2498.3ms
Speed: 5.4ms preprocess, 2498.3ms inference, 1.9ms postprocess per image at shape (1, 3, 448, 640)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
