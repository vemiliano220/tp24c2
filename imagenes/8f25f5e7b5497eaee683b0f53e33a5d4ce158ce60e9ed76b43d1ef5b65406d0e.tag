Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/8f25f5e7b5497eaee683b0f53e33a5d4ce158ce60e9ed76b43d1ef5b65406d0e.jpg: 448x640 1 person, 2265.4ms
Speed: 3.5ms preprocess, 2265.4ms inference, 2.2ms postprocess per image at shape (1, 3, 448, 640)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
