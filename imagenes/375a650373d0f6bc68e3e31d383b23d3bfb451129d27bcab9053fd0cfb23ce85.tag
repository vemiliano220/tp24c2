Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/375a650373d0f6bc68e3e31d383b23d3bfb451129d27bcab9053fd0cfb23ce85.jpg: 640x448 1 dog, 2230.2ms
Speed: 3.6ms preprocess, 2230.2ms inference, 2.9ms postprocess per image at shape (1, 3, 640, 448)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
