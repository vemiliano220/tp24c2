Ultralytics YOLOv8.2.48 🚀 Python-3.12.3 torch-2.3.1+cpu CPU (Intel Core(TM) i3-1005G1 1.20GHz)
YOLOv8l summary (fused): 268 layers, 43668288 parameters, 0 gradients, 165.2 GFLOPs

image 1/1 /imagenes/65a3f636e9a58cc2f658199bd1f731d9583b2fa183074f2e83aa3b3bb376a960.jpg: 640x448 1 clock, 2416.1ms
Speed: 4.3ms preprocess, 2416.1ms inference, 4.3ms postprocess per image at shape (1, 3, 640, 448)
Results saved to [1m/usr/src/app[0m
💡 Learn more at https://docs.ultralytics.com/modes/predict
