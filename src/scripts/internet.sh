
#!/bin/bash
# Función para verificar la conexión a Internet
check_internet_connection() {
    ping -c 1 google.com > /dev/null 2>&1
    if [ $? -eq 0 ]; then
        echo "La conexión a Internet está activa."
    else
        echo "No hay conexión a Internet."
    fi
}

# Llamada a la función para verificar la conexión
check_internet_connection


