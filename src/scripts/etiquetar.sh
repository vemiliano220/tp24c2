#!/bin/bash
export USE_NNPACK=0
# Itera sobre todas las imágenes .jpg en el directorio
for imagen in /imagenes/*.jpg; do
    if [ -f "$imagen" ]; then
        # Nombre base de la imagen sin la extensión .jpg
	    nombre_base=$(basename "$imagen" .jpg)
	    yolo predict source="$imagen" > "$nombre_base".tag	
        grep 'image' "$nombre_base".tag | cut -d ' ' -f 5- > "$nombre_base".tag
        if [ $? -eq 0 ]; then
            echo "Imagen $imagen etiquetada correctamente."
        else
            echo "Error al etiquetar la imagen $imagen."
        fi
    fi
done

