#!/bin/bash
RUTA=/../../imagenes/

# Nombre del archivo comprimido
ARCHIVO_COMPRIMIDO="imagenes_comprimidas.tar.gz"

# Archivo para la suma de verificación

ARCHIVO_SUMAS="imagenes_comprimidas.md5"

tar -czvf "$ARCHIVO_COMPRIMIDO" ${RUTA} 

#md5sum ${ARCHIVO_COMPRIMIDO} >  ${ARCHIVO_SUMAS} 

if [ -f ${RUTA}${ARCHIVO_COMPRIMIDO} ]; then
    echo "EXITO  al comprimir el contenido del directorio."
    exit 0
else 
	echo "un error, no se genero correctamente"
fi

md5sum $ARCHIVO_COMPRIMIDO > ${RUTA} $ARCHIVO_SUMAS"

if [ $? -ne 0 ]; then
    echo "Error al generar la suma de verificación."
    exit 1
fi

echo "Proceso completado: Archivo comprimido y suma de verificación generados."

