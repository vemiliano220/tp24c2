#!/bin/bash

# Ruta donde se guardará la imagen
ruta_de_guardado="./imagenes"

# Nombre del archivo temporal
archivo_temp="messi.jpg"

# URL de la imagen (debes reemplazar esta URL con la URL real de la imagen)
url="https://image.pollinations.ai/prompt/messi?seed=1"

# Descargar la imagen usando wget
wget -q --show-progress -O "$ruta_de_guardado/$archivo_temp" "$url"

# Comprobar si el archivo se ha creado
if [ -f "$ruta_de_guardado/$archivo_temp" ]; then
    echo "La imagen de Messi se ha descargado correctamente."
else
    echo "Hubo un problema al descargar la imagen de Messi."
fi


