#!/bin/bash

# URL base para descargar imágenes
BASE_URL="https://image.pollinations.ai/prompt"

# Ruta donde se guardará la imagen
RUTA_DE_GUARDADO="./imagenes"

# Nombre del archivo de imagen basado en su suma de verificación
NOMBRE_ARCHIVO=""

# Verifica si la carpeta de imágenes existe, si no, créala
if [ ! -d "$RUTA_DE_GUARDADO" ]; then
    mkdir -p "$RUTA_DE_GUARDADO"
fi
# Verifica si se proporcionó una clase
if [ -z "$1" ]; then
    echo "Uso: $0 <clase>"
    echo "Ejemplo: $0 person"
    exit 1
fi

# Clase de la imagen a descargar
CLASE=$1
# URL completa para descargar la imagen
URL="${BASE_URL}/a%20photo%20of%20a%20${CLASE}?seed=1"

# Descargar la imagen usando wget
wget -q --show-progress -O "$RUTA_DE_GUARDADO/${CLASE}.jpg" "$URL"
# Verificar si la descarga fue exitosa
if [ $? -eq 0 ]; then
    # Obtener la suma de verificación del archivo
    NOMBRE_ARCHIVO=$(md5sum "$RUTA_DE_GUARDADO/${CLASE}.jpg" | awk '{ print $1 }').jpg
    # Renombrar el archivo a su suma de verificación
    mv "$RUTA_DE_GUARDADO/${CLASE}.jpg" "$RUTA_DE_GUARDADO/$NOMBRE_ARCHIVO"
    echo "La imagen se ha descargado y renombrado a $NOMBRE_ARCHIVO correctamente."
else
    echo "Ha ocurrido un error al descargar la imagen."
    exit 1
fi

exit 0
