#!/bin/bash
IMAGENES=/../../imagenes/
# Solicitar al usuario que ingrese la etiqueta que desea buscar
read -p "Ingrese la etiqueta que desea buscar: " etiqueta_busqueda
#echo ${IMAGENES}.'/tag_file 
# Verificar si se ingresó una etiqueta
if [ -z "$etiqueta_busqueda" ]; then
    echo "No se ingresó ninguna etiqueta. Saliendo..."
    exit 1
fi

# Buscar en todos los archivos .tag para encontrar imágenes con la etiqueta buscada
encontrado=false
for tag_file in *.tag; do
    if [ -f "$tag_file" ]; then
        # Leer el contenido del archivo de etiquetas y buscar la etiqueta
        if grep -q "$etiqueta_busqueda" "$tag_file"; then
            # Obtener el nombre de la imagen correspondiente
            imagen=$(basename "$tag_file" .tag).jpg
	    jp2a ${IMAGENES}${imagen}
            # Mostrar la imagen    
            echo "Imagen con la etiqueta '$etiqueta_busqueda': $imagen"
            encontrado=true
        fi
    fi
done

# Informar si no se encontraron imágenes con la etiqueta
if [ "$encontrado" = false ]; then
    echo "No se encontraron imágenes con la etiqueta '$etiqueta_busqueda'."
fi

exit 0





# este script debe ejecurar un programa interactivo que no recibe argumentos.
# Debe preguntarle al usuario que etiqueta desea buscar y mostrar por
# pantalla todas las imágenes que tengan esa etiqueta.
