# syntax = edrevo/dockerfile-plus
INCLUDE+  .Dockerfile.base

# Instalar los programas necesarios
RUN apt-get install -y wget iputils-ping jp2a --fix-missing
# Configuracion de la aplicación
ENV TERM=xterm
ENV COLORTERM=24bit
COPY ["src/", "/app/"]
WORKDIR /app
ENTRYPOINT ["/app/main.sh"]

